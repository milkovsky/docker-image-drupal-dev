FROM php:7.0-apache

WORKDIR /var/www/html

COPY configs/php.ini /usr/local/etc/php/php.ini
COPY configs/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN chown -R www-data:www-data /var/www/html \
   && apt-get update && apt-get install -y libpng12-dev libjpeg-dev libpq-dev mysql-client patch wget redis-tools libfontconfig libxslt-dev lsof git git-core libbz2-dev \
   && rm -rf /var/lib/apt/lists/* \
   && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
   && docker-php-ext-install gd mbstring opcache pdo pdo_mysql pdo_pgsql zip xsl bz2 \
   && php -r "readfile('https://s3.amazonaws.com/files.drush.org/drush.phar');" > /usr/local/bin/drush \
   && chmod +x /usr/local/bin/drush \
   && curl -fSL "https://getcomposer.org/download/1.3.0/composer.phar" -o /usr/local/bin/composer \
   && chmod +x /usr/local/bin/composer \
   && curl -fSL "https://drupalconsole.com/installer" -o /usr/local/bin/drupal \
   && chmod +x /usr/local/bin/drupal \
   && pecl install xdebug-2.5.0 && docker-php-ext-enable xdebug
